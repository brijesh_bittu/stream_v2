Make sure you have installed redis, mongodb, nodejs, python & git

After installation, make sure that redis server and mongodb server is running.

Make a virtual environment (ex: `stream`)

1st command window

- `git clone https://brijesh_bittu@bitbucket.org/brijesh_bittu/stream_v2.git`
- `cd stream_v2`
- `workon stream`
- `pip install requirements.txt`
- `npm install -g bower`
- `npm install -g gulp`
- `bower install`
- `npm install`
- `python application.py`


2nd command window

- `workon stream`
- `huey_consumer.py be.queue.queue --logfile=./logs/huey.log`

3rd command window

- `gulp`