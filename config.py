import os

# from huey import RedisHuey
basedir = os.path.abspath(os.path.dirname(__file__))

# queue = RedisHuey("indexer", host="localhost", port=6379)


class Config:
    APP_NAME = 'Stream'
    APP_DESC = 'All the news you can get.'
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'new-app-hello'
    REMEMBER_COOKIE_NAME = 'stream'
    CACHE_KEY_PREFIX = 'stream'
    CACHE_DEFAULT_TIMEOUT = 1800

    @staticmethod
    def init_app(app):
        pass


class DevConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, "stream-dev.db")


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, "stream-test.db")


class ProdConfig(Config):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DEV_DATABASE_URL') or \
        'sqlite:///' + os.path.join(basedir, "stream-prod.db")


config = {
    'dev': DevConfig,
    'test': TestConfig,
    'prod': ProdConfig,
    'default': DevConfig
}


class Settings:
    MAIL_HOST = 'smtp.mandrillapp.com'
    MAIL_PORT = 587
    MAIL_TO = ['brijeshb42@gmail.com']
    MAIL_FROM = 'admin@stream.com'
    MAIL_USERNAME = 'brijeshb42@gmail.com'
    MAIL_PASSWORD = 'Zuf0BPIIJOzY5_5Up-JxxQ'
    SQLALCHEMY_DATABASE_URI = config["default"].SQLALCHEMY_DATABASE_URI
