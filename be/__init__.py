import os

from flask import Flask, send_from_directory

from .connector import app_bp

app = Flask(__name__)
app.register_blueprint(app_bp, url_prefix='/api')

TEMPLATE_DIR = os.path.join(
    os.path.dirname(os.path.dirname(__file__)), "fe")


@app.route('/<path:filename>')
def serve_files(filename=None):
    if not filename or filename == '' or filename == '/':
        filename = 'index.html'
    elif '/' in filename and '.' not in filename:
        filename = 'index.html'
    return send_from_directory(TEMPLATE_DIR, filename)


@app.route('/')
@app.route('/<string:path>')
def serve_index(path=None):
    return send_from_directory(TEMPLATE_DIR, 'index.html')
