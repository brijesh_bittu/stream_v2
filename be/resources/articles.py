from flask import abort
from flask_restful import Resource

from ..data import get_article


class Article(Resource):

    def get(self, aid):
        art = get_article(aid)
        if not art:
            abort(404)
        return art

    # def put(self, cid):
    #     cat[str(cid)] = 'Put'
    #     return {cid: cat[str(cid)]}


class Articles(Resource):

    def get(self):
        news = get_article()
        return news
