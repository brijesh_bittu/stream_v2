from flask import abort
from flask_restful import Resource, reqparse

from ..data import get_category, set_category


def valid_category(cat=None):
    if not cat or len(cat) < 1:
        raise ValueError('Provide a valid category')

parser = reqparse.RequestParser(bundle_errors=True)
parser.add_argument(
    'category', type=valid_category,
    location='form',
    required=True, help='Provide a name')


class Category(Resource):

    def get(self, cid):
        cat = get_category(cid)
        if not cat:
            abort(404)
        return cat

    # def put(self, cid):
    #     cat[str(cid)] = 'Put'
    #     return {cid: cat[str(cid)]}


class Categories(Resource):

    def get(self):
        cats = get_category()
        return cats

    def post(self):
        args = parser.parse_args()
        ins_id = set_category(value=args['category'])
        return {
            '_id': ins_id,
            'name': args['category']
        }
