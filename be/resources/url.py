import re

from flask import abort
from flask_restful import Resource, reqparse

from ..data import get_url, set_url, delete_url

REGEX_URL = re.compile(
    r'^[a-z]+://(?P<host>[^/:]+)(?P<port>:[0-9]+)?(?P<path>\/.*)?$')


def valid_URL(url=None):
    if not url:
        raise ValueError('Provide a valid URL')
    if not REGEX_URL.match(url):
        raise ValueError('{} is not a valid URL'.format(url))
    else:
        return url


parser = reqparse.RequestParser(bundle_errors=True)
parser.add_argument(
    'url', type=valid_URL,
    location='form',
    required=True, help='Provide a valid url(with http:// or https://)')
parser.add_argument(
    'name', type=str,
    location='form',
    required=True, help='Provide the website name')
parser.add_argument(
    'category', type=str,
    location='form',
    required=True, help='Provide a category id.')


class URL(Resource):

    def get(self, cid):
        return get_url(catid=cid)

    def delete(self, cid):
        return {'ok': delete_url(_id=cid)}

    # def put(self, cid):
    #     url[str(cid)] = 'Put'
    #     return {cid: url[str(cid)]}


class URLs(Resource):

    def get(self):
        urls = get_url()
        return urls

    def post(self):
        args = parser.parse_args()
        _id = set_url(
            category=args['category'].lower(),
            name=args['name'],
            value=args['url'])
        if not _id:
            abort(401)
        else:
            return {
                '_id': _id,
                'value': args['url'],
                'name': args['name']
            }
