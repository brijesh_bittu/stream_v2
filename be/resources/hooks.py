from flask_restful import Resource, reqparse

from ..queue import all_site

parser = reqparse.RequestParser(bundle_errors=True)
parser.add_argument(
    'type', type=str,
    location='args',
    required=True, help='Provide a hook type')


class Hook(Resource):

    def get(self):
        args = parser.parse_args()
        all_site(typ=args['type'])
        return {'ok': True}
