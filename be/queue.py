from huey import RedisHuey

from config import Config

from .content import get_all, get_by_cat

queue = RedisHuey(Config.APP_NAME, host="localhost", port=6379)


@queue.task()
def all_site(typ=None):
    """Long running task to run in background to get Articles."""
    if not typ:
        return
    if typ == 'all':
        get_all()
    else:
        get_by_cat(typ)
