import datetime

import requests
import newspaper

from .data import get_url, news

"""
config = Config()
config.memoize_articles = False
config.browser_user_agent = "Mozilla/5.0 (Macintosh; Intel
     Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko)
     Chrome/43.0.2357.124 Safari/537.36"
"""


def construct_article(article, likes, rt):
    article_dict = {
        "_id": article.link_hash,
        "title": article.title,
        "authors": article.authors,
        "text": article.text,
        "summary": article.summary,
        "image": article.top_image,
        "videos": article.movies,
        "keywords": article.keywords,
        "url": article.url,
        "date": datetime.datetime.now(),
        "likes": likes,
        "retweets": rt
    }
    return article_dict


def get_articles(url):
    """Get all articles of a particular website."""
    paper = newspaper.build(url)
    for article in paper.articles:
        if "feedsportal" in article.url or "blog." in article.url:
            continue
        article.download()
        article.parse()
        if article.html == "":
            continue
        if article.text == "" or "error" in article.title.lower():
            continue
        article.nlp()
        art_likes = requests.get(
            "https://graph.facebook.com/fql?"
            "q=SELECT url, normalized_url, share_count, like_count,"
            " comment_count, total_count, commentsbox_count,"
            " comments_fbid, click_count FROM link_stat"
            " WHERE url='%s'" % article.url)
        if art_likes.status_code == 200:
            try:
                art_likes = art_likes.json()["data"][0]["like_count"]
            except Exception:
                art_likes = 0
        else:
            art_likes = 0

        twt_rt = requests.get(
            "http://urls.api.twitter.com/1/urls/count.json"
            "?url=%s" % article.url)
        if twt_rt.status_code == 200:
            try:
                twt_rt = twt_rt.json()["count"]
            except Exception:
                twt_rt = 0
        else:
            twt_rt = 0
        news.insert_one(
            construct_article(
                article,
                art_likes,
                twt_rt
            )
        )
    print('Over')


def get_all():
    urls = get_url()
    for url in urls:
        get_articles(url['value'])


def get_by_cat(cat):
    urls = get_url(catid=cat)
    for url in urls:
        get_articles(url['value'])
