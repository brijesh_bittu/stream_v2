'''Mongo operations.'''
import datetime

from pymongo import MongoClient, DESCENDING
from bson import ObjectId
import tldextract

from .utilities import user_friendly_number

DB_STORE = 'stream_v2'
COLLECTION_NEWS = 'news'
COLLECTION_CATEGORIES = 'categories'
COLLECTION_URLS = 'urls'

client = MongoClient()

db = client[DB_STORE]
news = db[COLLECTION_NEWS]
categories = db[COLLECTION_CATEGORIES]
urls = db[COLLECTION_URLS]


def fix_date(cat):
    if 'added' in cat:
        cat['added'] = cat['added'].isoformat()
    if 'updated' in cat:
        cat['updated'] = cat['updated'].isoformat()
    return cat


def add_meta(article):
    # article['likes'] = user_friendly_number(article['likes'])
    # article['retweets'] = user_friendly_number(article['retweets'])
    article['domain'] = tldextract.extract(article['url']).domain
    article['date'] = article['date'].isoformat()
    article['likes'] = user_friendly_number(article['likes'])
    return article


def get_article(_id=None):
    if not _id:
        data = []
        for art in news.find({}, {
                'title': 1,
                'url': 1,
                'summary': 1,
                'likes': 1,
                'retweets': 1,
                'date': 1,
                'keywords': 1,
                'image': 1
                }).sort('likes', DESCENDING).limit(25):
            data.append(add_meta(art))
        return data
    article = news.find_one({'_id': _id})
    if article:
        return add_meta(article)
    return None


def get_category(_id=None):
    if not _id:
        cats = []
        for cat in categories.find():
            cats.append(fix_date(cat))
        return cats
    cat = categories.find_one({'_id': _id})
    if cat:
        return fix_date(cat)
    return None


def set_category(_id=None, value=None):
    if not value:
        return None
    if not _id:
        _id = value.lower().replace(' ', '')
    exists = categories.find({'_id': _id}).limit(1)
    if exists.count() == 1:
        exists.close()
        return _id
    return categories.insert_one({
        '_id': _id,
        'added': datetime.datetime.utcnow(),
        'name': value
        }).inserted_id


def get_url(catid=None):
    data = []
    if not catid:
        url = urls.find()
    else:
        url = urls.find({'catid': catid})
    for ur in url:
        ur['_id'] = str(ur['_id'])
        data.append(ur)
    return data


def set_url(category=None, value=None, name=None):
    if not category or not value:
        return None
    cat = categories.find_one({'_id': category})
    if cat:
        iid = urls.insert_one({
            'catid': category,
            'name': name,
            'value': value
        }).inserted_id
        return str(iid)
    return None


def delete_url(_id=None):
    if not _id:
        return False
    urls.remove({'_id': ObjectId(_id)})
    return True
