from flask import Blueprint
from flask_restful import Api

from .resources.articles import Article, Articles
from .resources.hooks import Hook
from .resources.category import Category, Categories
from .resources.url import URL, URLs

app_bp = Blueprint('api', __name__)
api = Api(app_bp)


@app_bp.errorhandler(404)
def page_not_found(e):
    return {'message': 'Not Found', 'status': 404}, 404

api.add_resource(Article, '/a/<string:aid>')
api.add_resource(Articles, '/a/')
api.add_resource(Category, '/c/<string:cid>')
api.add_resource(Categories, '/c/')
api.add_resource(URL, '/url/<string:cid>')
api.add_resource(URLs, '/url/')
api.add_resource(Hook, '/h')
