import math


def user_friendly_number(val):
    if val < 1000:
        return str(val)
    frm = 'KMBTPE'
    formatter = '%.1f%s'
    exp = int(math.log10(val)/math.log10(1000.0))
    return (formatter % (val/math.pow(1000, exp), frm[exp-1])).replace(
        '.0', '')
