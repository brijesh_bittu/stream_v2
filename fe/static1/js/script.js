(function($) {

    $('#crawl-urls').on('click', function(e) {
        e.preventDefault();
        $.get('/api/h?type=all');
    });

    function NavModel() {
        var self = this;
        this.categories = ko.observableArray([]);

        this.addCategory = function(ele) {
            var $this = $(ele);
            $.post('/api/c/', $this.serialize(), function(data) {
                self.categories.push(data);
                $this.each(function() {
                    this.reset();
                });
            }).fail(function(a, b, c) {
                alert("Error");
            });
        };
    }

    function ArticleModel() {
        this.articles = ko.observableArray([]);
    }


    function UrlModel(cat) {
        var self = this;
        this.category = ko.observable('');
        this.urls = ko.observableArray([]);
        this.bond = false;

        this.saveUrl = function(ele) {
            var $form = $(ele);
            $.post('/api/url/', $form.serialize(), function(data){
                self.urls.push(data);
                $form.each(function() {
                    this.reset();
                });
            })
        };

        this.deleteUrl = function() {
            var $this = this;
            $.ajax({
                url: '/api/url/'+$this['_id'],
                type: 'DELETE',
                success: function(result) {
                    if(result.ok && result.ok === true) {
                        self.urls.remove($this);
                    }
                }
            });
        };
    }

    var navs = new NavModel();
    var urls = new UrlModel();
    var articles = new ArticleModel();

    page('/', function() {
        if(navs.categories().length > 0) {
            return;
        }
        $.get('/api/c/', function(data) {
            navs.categories(data);
        });
    });

    page('/articles', function() {
        if(articles.articles().length > 0) {
            return;
        }
        $.get('/api/a/', function(data) {
            articles.articles(data);
            ko.applyBindings(articles, document.getElementById("articles"));
        });
    });

    page('/c/:cid', function(ctx) {
        var cid = ctx.params.cid;
        $.get('/api/url/'+cid, function(data) {
            urls.urls(data);
            urls.category(cid);
            if(!urls.bond) {
                ko.applyBindings(urls, document.getElementById("urls"));
                urls.bond = true;
            }
        });
    });

    page();

    /*$(document).ready(function() {
        $("#new-category-form").on("submit", function(e) {
            e.preventDefault();
            var $this = $(this);
            $.post('/api/c/', $this.serialize(), function(data) {
                navs.categories.push(data);
                $this.each(function() {
                    this.reset();
                });
            });
        });
    });*/

    ko.applyBindings(navs, document.getElementById("navigation"));
})(jQuery);
